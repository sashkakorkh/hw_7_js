/*Exercise 1*/
/*1. Метод масиву forEach дозволяє застосувати якусь функцію(дію) для усіх елементів масиву по черзі
* 2. arr.length = 0
* 3. Array.isArray(arr) повертає true якщо змінна це масив */

let arr = [1,2,6,true,false, 'get', null, {'name': 'Sasha', 'lastName': 'Korkh'}]

/*
let result = [];
function filterBy (array, datatype) {
    array.forEach(item => {
        if (typeof item !== datatype || datatype === 'object' && item === null) {
            result.push(item);
        }
        if(datatype === 'null' && item === null) {
            result.pop(item);
        }
})
    return result
}
*/



function filterBy (array, datatype) {
    let result = array.filter(item => {
        return (typeof item !== datatype || datatype === 'object' && item === null);
    });
     /*if(datatype === 'null' && result.includes( null)) {

     }*/
    return result;
}



console.log(filterBy(arr, 'null'));

